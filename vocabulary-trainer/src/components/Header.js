import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import "./Header.scss";

export class Header extends Component {

  constructor(props) {
    super();

    this.state = {

    };
  } // end constructor

  render() {
    return (
        <header className="App-header row">
            <div className="col-12">
                <h1><Link to="/">Vokabeltrainer</Link></h1>
            </div>
      </header>
    );
  } // end render

} // end Header
