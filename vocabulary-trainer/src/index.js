import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './index.scss';
import LangSelect from './LangSelect';
import LangDashboard from './LangDashboard';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={LangSelect} />
            <Route path="/dashboard/italienisch" component={LangDashboard} />
            <Route path="/dashboard/französisch" component={LangDashboard} />
            <Route component={LangSelect}/>
        </Switch>
    </BrowserRouter>
), document.getElementById('root'));

registerServiceWorker();