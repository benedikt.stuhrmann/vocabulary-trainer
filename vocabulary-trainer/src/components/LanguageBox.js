import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import "./LanguageBox.scss";
import { Progressbar } from "./Progressbar";

const flags = require.context("../flags", true);

export class LanguageBox extends Component {

  constructor(props) {
    super();

    this.state = {
      flag: flags("./flag_" + props.langKey + ".png"),
      flag_alt: props.language + "e Flagge",
      link: "/dashboard/" + props.language.toLowerCase()
    };

  } // end constructor

  render() {
    return (
      <div className="col-12 col-md-6 col-lg-4">
        <Link to={this.state.link} className="language-box-link">
          <div className="language-box col-12 no-padding">
            <div className="basic-info col-12 no-padding">
              <div className="flag">
                <img src={this.state.flag} alt={this.state.flag_alt} />
              </div>
              <div className="lang-info">
                <h2>{this.props.language}</h2>
                <span>Vokabeln: {this.props.stats.total}</span>
              </div>
            </div>
            <div className="detail-info col-12 no-padding">
              Fortschritt:
              <Progressbar total={this.props.stats.total} 
                           unlearned={this.props.stats.unlearned} 
                           step1={this.props.stats.step1} 
                           step2={this.props.stats.step2} 
                           learned={this.props.stats.learned}/>
            </div>
          </div>
        </Link>
      </div>
    );
  } // end render

} // end ListEntry
