import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Progressbar.scss";

export class Progressbar extends Component {

  constructor(props) {
    super();

    let factor = 100/props.total;

    this.state = {
        unlearned_perc: factor * props.unlearned,
        step1_perc:     factor * props.step1,
        step2_perc:     factor * props.step2,
        learned_perc:   factor * props.learned
    };
  } // end constructor

  render() {
    return (
      <div className="progressbar">
        <div className="unlearned" style={{width: this.state.unlearned_perc + "%"}}></div>
        <div className="step1"     style={{width: this.state.step1_perc + "%"}}></div>
        <div className="step2"     style={{width: this.state.step2_perc + "%"}}></div>
        <div className="learned"   style={{width: this.state.learned_perc + "%"}}></div>
      </div>
    );
  } // end render

} // end Progessbar
