import React, { Component } from "react";
import { Header } from "./components/Header";
import { LanguageBox } from "./components/LanguageBox";
import { CreateLanguage } from "./components/CreateLanguage";

class LangSelect extends Component {

  render() {
    const languages = [
      {
        key: "italian",
        name: "Italienisch",
        stats: {
          total:     490,
          unlearned: 173,
          step1:     22,
          step2:     77,
          learned:   218
        }
      },
      {
        key: "french",
        name: "Französisch",
        stats: {
          total:     42,
          unlearned: 22,
          step1:     3,
          step2:     8,
          learned:   10
        }
      }
    ];

    return (
      <div className="App container">

        <Header />

        <main className="row">
          {languages.map( (lang) => <LanguageBox key={lang.key} langKey={lang.key} language={lang.name} stats={lang.stats}/>)}
          <CreateLanguage />
        </main>

        <footer>

        </footer>
      </div>
    );
  }
}

export default LangSelect;
