import React, { Component } from "react";
import "./LangDashboard.scss"
import { Header } from "./components/Header";

class LangDashboard extends Component {

  constructor(props) {
    super();

    // get the requested dashboard language
    let path_parts = props.location.pathname.split("/");
    let requested_dashboard = path_parts[path_parts.indexOf("dashboard") + 1];

    this.state = {
      selected_lang: requested_dashboard
    };

  } // end constructor

  render() {
    return (
      <div className="App container">

        <Header />

        <main className="row">
          <div>
            <h2>Dashboard {this.state.selected_lang}</h2>
          </div>
        </main>

        <footer>

        </footer>
      </div>
    );
  }
}

export default LangDashboard;
