import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import "./LanguageBox.scss";
import "./CreateLanguage.scss";

const flags = require.context("../flags", true);

export class CreateLanguage extends Component {

  constructor(props) {
    super();

    this.state = {
    };

  } // end constructor

  render() {
    return (
      <div className="col-12 col-md-6 col-lg-4">
        <Link to="neue-sprache-lernen" className="language-box-link">
            <div className="language-box create-new-box col-12 no-padding">
                <div className="flags">
                    <div className="flag">
                        <img src={flags("./flag_italian.png")} />
                    </div>
                    <div className="flag">
                        <img src={flags("./flag_french.png")} />
                    </div>
                    <div className="flag">
                        <img src={flags("./flag_italian.png")} />
                    </div>
                </div>
                <h2>Neue Sprache lernen</h2>
            </div>
        </Link>
      </div>
    );
  } // end render

} // end CreateLanguage
